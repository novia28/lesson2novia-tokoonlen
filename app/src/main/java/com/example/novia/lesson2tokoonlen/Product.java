package com.example.novia.lesson2tokoonlen;

public class Product {
    public int _id;
    public String nama;
    public String detail;
    public Integer harga;
    public Integer gambar;
    public boolean status;

    public Product() {

    }

    public Product(int _id, String nama, String detail, Integer harga, Integer gambar, boolean status) {
        this._id = _id;
        this.nama = nama;
        this.detail = detail;
        this.harga = harga;
        this.gambar = gambar;
        this.status = status;
    }

    public int getID() {
        return this._id;
    }

    public void setID(int _id) {
        this._id = _id;
    }

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getHarga() {
        return this.harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getGambar() {
        return this.gambar;
    }

    public void setGambar(Integer gambar) {
        this.gambar = gambar;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
