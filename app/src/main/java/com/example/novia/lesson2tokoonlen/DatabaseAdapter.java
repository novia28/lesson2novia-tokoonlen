package com.example.novia.lesson2tokoonlen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseAdapter {
    public static final String COL_ID = "_id";
    public static final String COL_NAMA = "nama";
    public static final String COL_DETAIL = "detail";
    public static final String COL_HARGA = "harga";
    public static final String COL_GAMBAR = "gambar";
    public static final String COL_STATUS = "status";

    private static final String TAG = "DatabaseAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private static final String DATABASE_NAME = "toko_onlen";
    private static final String TABLE_NAME = "product";
    private static final int DATABASE_VERSION = 1;

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nama TEXT, detail TEXT, " +
                    "harga TEXT, gambar TEXT, status TEXT)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }

    public DatabaseAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long createProduct(String nama, String detail, Integer harga, Integer gambar, Boolean status) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(COL_NAMA, nama);
        initialValues.put(COL_DETAIL, detail);
        initialValues.put(COL_HARGA, harga);
        initialValues.put(COL_GAMBAR, gambar);
        initialValues.put(COL_STATUS, status);

        return mDb.insert(TABLE_NAME, null, initialValues);
    }

    public boolean deleteAllProducts() {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        int doneDelete = 0;
        doneDelete = mDb.delete(TABLE_NAME, null, null);
        Log.w(TAG, Integer.toString(doneDelete));
        mDb.close();

        return doneDelete > 0;
    }

    public boolean updateData(String nama, boolean status) {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_STATUS, status);
        mDb.update(TABLE_NAME, contentValues, "nama = ?", new String[]{nama});
        mDb.close();

        return true;
    }

    public Cursor selectQuery(String query) {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        Cursor c1 = mDb.rawQuery(query, null);

        return c1;
    }

    public void insertProducts() {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        createProduct("Jaket 1", "Jaket 1", 210000, R.drawable.jaket1, false);
        createProduct("Jaket 2", "Jaket 2", 220000, R.drawable.jaket2, false);
        createProduct("Jaket 3", "Jaket 3", 230000, R.drawable.jaket3, false);
        createProduct("Jaket 4", "Jaket 4", 240000, R.drawable.jaket4, false);
        createProduct("Sepatu 1", "Sepatu 1", 250000, R.drawable.sepatu1, false);
        createProduct("Sepatu 2", "Sepatu 2", 260000, R.drawable.sepatu2, false);
        createProduct("Sepatu 3", "Sepatu 3", 270000, R.drawable.sepatu3, false);
        createProduct("Tas 1", "Tas 1", 270000, R.drawable.tas1, false);
        createProduct("Tas 2", "Tas 2", 270000, R.drawable.tas2, false);
        createProduct("Tas 3", "Tas 3", 270000, R.drawable.tas3, false);
        createProduct("Tas 4", "Tas 4", 270000, R.drawable.tas4, false);
        createProduct("Tas 5", "Tas 5", 270000, R.drawable.tas5, false);
        mDb.close();
    }
}
