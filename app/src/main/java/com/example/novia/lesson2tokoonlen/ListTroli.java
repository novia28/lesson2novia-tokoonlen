package com.example.novia.lesson2tokoonlen;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListTroli extends AppCompatActivity {
    private DatabaseAdapter dbHelper;
    ListView listViewTroli;
    TextView tvNama, tvDetail, tvHarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_troli);

        listViewTroli = findViewById(R.id.listViewTroli);
        tvNama = findViewById(R.id.namaTroli);
        tvDetail = findViewById(R.id.detailTroli);
        tvHarga = findViewById(R.id.hargaTroli);

        dbHelper = new DatabaseAdapter(this);
        showList();
    }

    private void showList() {
        ArrayList<Product> troliList = new ArrayList<>();
        troliList.clear();
        String query = "SELECT * FROM product WHERE status=1 ";
        Cursor c1 = dbHelper.selectQuery(query);
        if (c1 != null && c1.getCount() != 0) {
            if (c1.moveToFirst()) {
                do {
                    Product troli = new Product();

                    troli.setNama(c1.getString(c1
                            .getColumnIndex("nama")));
                    troli.setDetail(c1.getString(c1
                            .getColumnIndex("detail")));
                    troli.setHarga(c1.getInt(c1
                            .getColumnIndex("harga")));
                    troli.setGambar(c1.getInt(c1
                            .getColumnIndex("gambar")));
                    troliList.add(troli);

                } while (c1.moveToNext());
            }
        }
        c1.close();

        TroliAdapter troliAdapter = new TroliAdapter(
                ListTroli.this, troliList);
        listViewTroli.setAdapter(troliAdapter);
    }
}
