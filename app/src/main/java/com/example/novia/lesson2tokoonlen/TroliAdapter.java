package com.example.novia.lesson2tokoonlen;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class TroliAdapter extends BaseAdapter {
    Context context;
    ArrayList<Product> troliList;
    DatabaseAdapter myDb;

    static class ViewHolder {
        ImageView holderImgTroli;
        TextView holderNamaTroli;
        TextView holderDetailTroli;
        TextView holderHargaTroli;
        Button btBatal;
    }

    public TroliAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        troliList = products;
    }

    @Override
    public int getCount() {
        return troliList.size();
    }

    @Override
    public Object getItem(int position) {
        return troliList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Product troliListItems = troliList.get(position);
        final ViewHolder holder;
        myDb = new DatabaseAdapter(this.context);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_troli, null);

            holder = new ViewHolder();
            holder.holderNamaTroli = convertView.findViewById(R.id.namaTroli);
            holder.holderDetailTroli = convertView.findViewById(R.id.detailTroli);
            holder.holderHargaTroli = convertView.findViewById(R.id.hargaTroli);
            holder.holderImgTroli = convertView.findViewById(R.id.imgTroli);
            holder.btBatal = convertView.findViewById(R.id.btBatalTroli);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.btBatal.setTag(position);
        holder.btBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isUpdate = myDb.updateData(troliListItems.getNama(), false);

                if (isUpdate == true) {
                    Log.d("Success", troliListItems.getNama());
                } else {
                    Log.d("Failed", troliListItems.getNama());
                }

                troliList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.holderNamaTroli.setText(troliListItems.getNama());
        holder.holderDetailTroli.setText(troliListItems.getDetail());

        DecimalFormatSymbols format = new DecimalFormatSymbols();
        format.setCurrencySymbol("Rp. ");
        format.setGroupingSeparator('.');
        format.setMonetaryDecimalSeparator(',');
        DecimalFormat idr = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        idr.setDecimalFormatSymbols(format);
        holder.holderHargaTroli.setText(idr.format(troliListItems.getHarga()));
        holder.holderImgTroli.setImageResource(troliListItems.getGambar());

        myDb.close();

        return convertView;
    }

}
