package com.example.novia.lesson2tokoonlen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ProductDetail extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_product);

        initActivity();
    }

    public void initActivity(){
        String parsedDataNama = "";
        String parsedDataDetail = "";
        Integer parsedDataHarga;
        Integer parsedDataGambar;

        TextView nama = findViewById(R.id.namaDetailProduct);
        TextView detail = findViewById(R.id.detailDetailProduct);
        TextView harga = findViewById(R.id.hargaDetailProduct);
        ImageView gambar = findViewById(R.id.imgDetailProduct);

        if(getIntent().hasExtra("parse_nama")){
            parsedDataNama = getIntent().getExtras().getString("parse_nama");
            nama.setText(parsedDataNama);
        }else{
            nama.setText("No Parsed Input");
        }

        if(getIntent().hasExtra("parse_detail")){
            parsedDataDetail = getIntent().getExtras().getString("parse_detail");
            detail.setText(parsedDataDetail);
        }else{
            detail.setText("No Parsed Input");
        }

        if(getIntent().hasExtra("parse_harga")){
            parsedDataHarga = getIntent().getExtras().getInt("parse_harga");
            DecimalFormatSymbols format = new DecimalFormatSymbols();
            format.setCurrencySymbol("Rp. ");
            format.setGroupingSeparator('.');
            format.setMonetaryDecimalSeparator(',');
            DecimalFormat idr = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            idr.setDecimalFormatSymbols(format);
            harga.setText(idr.format(parsedDataHarga));
        }else{
            harga.setText("No Parsed Input");
        }

        if(getIntent().hasExtra("parse_gambar")){
            parsedDataGambar = getIntent().getExtras().getInt("parse_gambar");
            gambar.setImageResource(parsedDataGambar);
        }else{
            gambar.setImageResource(R.drawable.default_image);
        }
    }
}
