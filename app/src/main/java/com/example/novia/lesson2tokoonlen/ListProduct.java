package com.example.novia.lesson2tokoonlen;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListProduct extends AppCompatActivity {
    private DatabaseAdapter dbHelper;
    ListView listView;
    TextView tvNama, tvDetail, tvHarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_product);

        listView = findViewById(R.id.listViewProduct);
        tvNama = findViewById(R.id.namaProduct);
        tvDetail = findViewById(R.id.detailProduct);
        tvHarga = findViewById(R.id.hargaProduct);

        dbHelper = new DatabaseAdapter(this);
        showList();
    }

    private void showList() {
        ArrayList<Product> productList = new ArrayList<>();
        productList.clear();
        String query = "SELECT * FROM product ";
        Cursor c1 = dbHelper.selectQuery(query);
        if (c1 != null && c1.getCount() != 0) {
            if (c1.moveToFirst()) {
                do {
                    Product product = new Product();
                    boolean value = c1.getInt(5) > 0;

                    product.setNama(c1.getString(c1
                            .getColumnIndex("nama")));
                    product.setDetail(c1.getString(c1
                            .getColumnIndex("detail")));
                    product.setHarga(c1.getInt(c1
                            .getColumnIndex("harga")));
                    product.setGambar(c1.getInt(c1
                            .getColumnIndex("gambar")));
                    product.setStatus(value);

                    productList.add(product);

                } while (c1.moveToNext());
            }
        }
        c1.close();

        ProductsAdapter productsAdapter = new ProductsAdapter(
                ListProduct.this, productList);
        listView.setAdapter(productsAdapter);
    }

}
