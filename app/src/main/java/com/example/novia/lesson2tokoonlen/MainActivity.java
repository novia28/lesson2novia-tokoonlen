package com.example.novia.lesson2tokoonlen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private DatabaseAdapter dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DatabaseAdapter(this);
        dbHelper.deleteAllProducts();
        dbHelper.insertProducts();
    }

    public void goToLihatBarang(View view){
        Intent intent = new Intent(view.getContext(), ListProduct.class);
        view.getContext().startActivity(intent);
    }

    public void goToLihatTroli(View view){
        Intent intent = new Intent(view.getContext(), ListTroli.class);
        view.getContext().startActivity(intent);
    }
}
