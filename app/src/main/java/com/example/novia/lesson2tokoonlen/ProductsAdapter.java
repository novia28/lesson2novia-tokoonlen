package com.example.novia.lesson2tokoonlen;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ProductsAdapter extends BaseAdapter {
    Context context;
    ArrayList<Product> productList;
    DatabaseAdapter myDb;

    static class ViewHolder {
        ImageView holderImgProduct;
        TextView holderNamaProduct;
        TextView holderDetailProduct;
        TextView holderHargaProduct;
        Button btPilih;
    }

    ProductsAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        productList = products;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        final Product productListItems = productList.get(position);
        final ViewHolder holder;
        myDb = new DatabaseAdapter(this.context);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_product, null);

            holder = new ViewHolder();
            holder.holderNamaProduct = convertView.findViewById(R.id.namaProduct);
            holder.holderDetailProduct = convertView.findViewById(R.id.detailProduct);
            holder.holderHargaProduct = convertView.findViewById(R.id.hargaProduct);
            holder.holderImgProduct = convertView.findViewById(R.id.imgProduct);
            holder.btPilih = convertView.findViewById(R.id.btPilihProduct);

            if (productListItems.getStatus()) {
                int warna = holder.btPilih.getResources().getColor(R.color.disabledbutton);
                holder.btPilih.setBackgroundColor(warna);
                holder.btPilih.setText("Batal");
            } else {
                int warna = holder.btPilih.getResources().getColor(R.color.orangeshopee);
                holder.btPilih.setBackgroundColor(warna);
                holder.btPilih.setText("Pilih");
            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            if (productListItems.getStatus()) {
                int warna = holder.btPilih.getResources().getColor(R.color.disabledbutton);
                holder.btPilih.setBackgroundColor(warna);
                holder.btPilih.setText("Batal");
            } else {
                int warna = holder.btPilih.getResources().getColor(R.color.orangeshopee);
                holder.btPilih.setBackgroundColor(warna);
                holder.btPilih.setText("Pilih");
            }
        }

        holder.btPilih.setTag(position);
        holder.btPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int warnaDisable = holder.btPilih.getResources().getColor(R.color.disabledbutton);
                int warnaOrange = holder.btPilih.getResources().getColor(R.color.orangeshopee);

                if (!productListItems.getStatus()) {
                    holder.btPilih.setBackgroundColor(warnaDisable);
                    holder.btPilih.setText("Batal");
                    myDb.updateData(productListItems.getNama(), true);
                    productListItems.status = true;
                } else {
                    holder.btPilih.setBackgroundColor(warnaOrange);
                    holder.btPilih.setText("Pilih");
                    myDb.updateData(productListItems.getNama(), false);
                    productListItems.status = false;
                }
            }
        });

        Button btDetailProduct = convertView.findViewById(R.id.btDetailProduct);
        btDetailProduct.setTag(position);
        btDetailProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ProductDetail.class);
                intent.putExtra("parse_nama", productListItems.nama);
                intent.putExtra("parse_detail", productListItems.detail);
                intent.putExtra("parse_harga", productListItems.harga);
                intent.putExtra("parse_gambar", productListItems.gambar);

                view.getContext().startActivity(intent);
            }
        });

        holder.holderNamaProduct.setText(productListItems.getNama());
        holder.holderDetailProduct.setText(productListItems.getDetail());

        DecimalFormatSymbols format = new DecimalFormatSymbols();
        format.setCurrencySymbol("Rp. ");
        format.setGroupingSeparator('.');
        format.setMonetaryDecimalSeparator(',');
        DecimalFormat idr = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        idr.setDecimalFormatSymbols(format);

        holder.holderHargaProduct.setText(idr.format(productListItems.getHarga()));
        holder.holderImgProduct.setImageResource(productListItems.getGambar());

        myDb.close();

        return convertView;
    }
}
